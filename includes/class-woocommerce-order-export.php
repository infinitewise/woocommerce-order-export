<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://faisalawan.me/
 * @since      1.1.0
 *
 * @package    Woocommerce_Order_Export
 * @subpackage Woocommerce_Order_Export/includes
 */

class Woocommerce_Order_Export {

	protected $loader;

	protected $plugin_name;

	protected $version;

	public function __construct() {
		if ( defined( 'WOE_VERSION' ) ) {
			$this->version = WOE_VERSION;
		} else {
			$this->version = '1.1.0';
		}
		$this->plugin_name = 'woocommerce-order-export';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_general_hooks();
		$this->define_admin_hooks();
		$this->define_public_hooks();
	}

	private function load_dependencies() {

		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'vendor/autoload.php';

		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-woocommerce-order-export-general.php';

		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-woocommerce-order-export-public.php';

		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-woocommerce-order-export-loader.php';

		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-woocommerce-order-export-i18n.php';

		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-woocommerce-order-export-admin.php';

		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-woocommerce-order-export-public.php';

		$this->loader = new Woocommerce_Order_Export_Loader();
		//$this->loader = new Woocommerce_Order_Export_General();

	}

	private function set_locale() {
		$plugin_i18n = new Woocommerce_Order_Export_i18n();
		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );
	}

	private function define_general_hooks() {
		$plugin_general = new Woocommerce_Order_Export_General( $this->get_plugin_name(), $this->get_version() );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_general, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_general, 'enqueue_scripts' );

		$this->loader->add_action( 'woocommerce_checkout_order_processed', $plugin_general, 'woocommerce_new_order', 10, 1 );

		$this->loader->add_filter( 'woocommerce_email_attachments', $plugin_general, 'attach_terms_conditions_pdf_to_email', 10, 3); 
	}

	private function define_admin_hooks() {
		$plugin_admin = new Woocommerce_Order_Export_Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action('admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action('admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
		$this->loader->add_action('admin_notices', $plugin_admin, 'check_wc' );
		$this->loader->add_action('admin_menu', $plugin_admin, 'add_submenu_page', 1000 );
		$this->loader->add_action('admin_init', $plugin_admin, 'page_init', 100, 1);
		$this->loader->add_filter('manage_edit-shop_order_columns', $plugin_admin, 'show_woocommerce_order_export_column', 15);
		$this->loader->add_action('manage_shop_order_posts_custom_column', $plugin_admin, 'woocommerce_order_export_column_callback', 10, 2);
		$this->loader->add_action('add_meta_boxes', $plugin_admin, 'woe_add_meta_boxes' );
		$this->loader->add_action('admin_init', $plugin_admin, 'register_session' );

		
	}

	private function define_public_hooks() {
		$plugin_public = new Woocommerce_Order_Export_Public( $this->get_plugin_name(), $this->get_version() );
		//$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		//$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );
	}

	public function run() {
		$this->loader->run();
	}

	public function get_plugin_name() {
		return $this->plugin_name;
	}

	public function get_loader() {
		return $this->loader;
	}

	public function get_version() {
		return $this->version;
	}

}
