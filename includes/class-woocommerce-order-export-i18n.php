<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       http://faisalawan.me/
 * @since      1.0.0
 *
 * @package    Woocommerce_Order_Export
 * @subpackage Woocommerce_Order_Export/includes
 */

class Woocommerce_Order_Export_i18n {

	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'woocommerce-order-export',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
