<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://faisalawan.me/
 * @since      1.0.0
 *
 * @package    Woocommerce_Order_Export
 * @subpackage Woocommerce_Order_Export/includes
 */

class Woocommerce_Order_Export_Deactivator {

	public static function deactivate() {

	}

}
