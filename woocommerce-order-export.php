<?php
/**
 *
 * @link              http://faisalawan.me/
 * @since             1.0.0
 * @package           Woocommerce_Order_Export
 *
 * @wordpress-plugin
 * Plugin Name:       WooCommerce Order Export
 * Plugin URI:        http://faisalawan.me/projects/woocommerce-order-export
 * Description:       WooCommerce Order Export.
 * Version:           1.0.0
 * Author:            Faisal Awan
 * Author URI:        http://faisalawan.me/
 * Text Domain:       woocommerce-order-export
 * Domain Path:       /languages
 */

define( 'WOE_VERSION', '1.1.0' );
define( 'WOE_PATH', plugin_dir_path( __FILE__ ) );
define( 'WOE_URL', plugin_dir_url( __FILE__ ) );

$files_path = wp_upload_dir();
$basedir_path = $files_path['basedir'];
$files_path = $basedir_path.DIRECTORY_SEPARATOR."woocommerce-order-reports".DIRECTORY_SEPARATOR;
define( 'WOE_REPORTS_PATH', $files_path );


function activate_woocommerce_order_export() {
        require_once plugin_dir_path( __FILE__ ) . 'includes/class-woocommerce-order-export-activator.php';
        new Woocommerce_Order_Export_Activator();
}

function deactivate_woocommerce_order_export() {
        require_once plugin_dir_path( __FILE__ ) . 'includes/class-woocommerce-order-export-deactivator.php';
        Woocommerce_Order_Export_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_woocommerce_order_export' );
register_deactivation_hook( __FILE__, 'deactivate_woocommerce_order_export' );

require plugin_dir_path( __FILE__ ) . 'includes/class-woocommerce-order-export.php';

function run_woocommerce_order_export() {

        $plugin = new Woocommerce_Order_Export();
        $plugin->run();

}
run_woocommerce_order_export();