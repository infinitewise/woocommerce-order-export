<?php

/**
 * Fired when the plugin is uninstalled.
 *
 * @link       http://faisalawan.me/
 * @since      1.0.0
 * @package    Woocommerce_Order_Export
 */

// If uninstall not called from WordPress, then exit.
if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	exit;
}