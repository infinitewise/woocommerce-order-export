<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://faisalawan.me/
 * @since      1.0.0
 *
 * @package    Woocommerce_Order_Export
 * @subpackage Woocommerce_Order_Export/public
 */

class Woocommerce_Order_Export_Public {

	private $plugin_name;

	private $version;

	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	public function enqueue_styles() {

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/woocommerce-order-export-public.css', array(), $this->version, 'all' );

	}

	public function enqueue_scripts() {

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/woocommerce-order-export-public.js', array( 'jquery' ), $this->version, false );

	}

}
